
//==================================================================================================//
//==================================================================================================//
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgReduxModule } from '@angular-redux/store';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { VATService } from '../../services/vat.service';
import { SearchComponent } from '../search/search.component';
import { DetailsComponent } from '../details/details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../../app.component';
import { NotificationComponent } from './notification.component';
//==================================================================================================//
//==================================================================================================//
/**
 * DetailsComponent - Test Suite
 */
describe('NotificationComponent', () => {
    let fixture: ComponentFixture<NotificationComponent>;
    let component: NotificationComponent;
    /**
     * Setup - Called before each tests
     */
    beforeEach(async(() => {
        //Inject configuration before every test
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                HttpClientModule,
                CommonModule,
                FormsModule,
                NgReduxModule,
                RouterTestingModule
            ],
            providers: [VATService],
            declarations: [
                AppComponent,
                SearchComponent,
                NotificationComponent,
                DetailsComponent,
            ]
        })
            .compileComponents().then(() => {
                fixture = TestBed.createComponent(NotificationComponent);
                component = fixture.componentInstance;
            });
    }));
    it('should create the app - NotificationComponent', async(() => {
        expect(component).toBeTruthy();
    }));
});

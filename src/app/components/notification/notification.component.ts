import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { VATService } from '../../services/vat.service';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import * as NotificationActions from "../../actions/notification.actions";
import { AppState } from '../../store/app.state';
import { ToastContainer } from 'app/models/toast.model';

import './notification.component.sass';

@Component({
    selector: "notification-component",
    templateUrl: './notification.component.html',
    encapsulation: ViewEncapsulation.None
})

export class NotificationComponent implements OnInit {
    vatSearchInput: string;

    @select('notificationContainer') notificationContainer$: Observable<ToastContainer>;
    notificationContainer: ToastContainer;

    constructor(private ngRedux: NgRedux<AppState>) { }

    ngOnInit() {
        this.notificationContainer$.subscribe((x: ToastContainer) => this.notificationContainer = x);
    }

    dismiss(id: string) {
        this.notificationContainer.notifications = this.notificationContainer.notifications.filter(function (obj) {
            return obj.id !== id;
        });
        let notificationContainerPayload: ToastContainer = {
            notifications: this.notificationContainer.notifications,
        };
        let notificationActions: NotificationActions.Actions = new NotificationActions.updateNotification(notificationContainerPayload);
        this.ngRedux.dispatch({ type: notificationActions.type, payload: notificationActions.payload });
    }
}
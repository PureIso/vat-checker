import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { SearchFilter } from 'app/models/search.model';

import './details.component.sass';

@Component({
    selector: "details-component",
    templateUrl: './details.component.html',
    encapsulation: ViewEncapsulation.None
})

export class DetailsComponent implements OnInit {
    vatSearchInput: string;

    @select('searchFilter') searchFilter$: Observable<SearchFilter>;
    searchFilter: SearchFilter;

    constructor() { }

    ngOnInit() {
        this.searchFilter$.subscribe((x: SearchFilter) => this.searchFilter = x);
    }
}
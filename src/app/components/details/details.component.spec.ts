
//==================================================================================================//
//==================================================================================================//
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgReduxModule } from '@angular-redux/store';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { VATService } from '../../services/vat.service';
import { SearchComponent } from '../../components/search/search.component';
import { DetailsComponent } from '../details/details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../../app.component';
import { NotificationComponent } from '../notification/notification.component';
//==================================================================================================//
//==================================================================================================//
/**
 * DetailsComponent - Test Suite
 */
describe('DetailsComponent', () => {
    let fixture: ComponentFixture<DetailsComponent>;
    let component: DetailsComponent;
    /**
     * Setup - Called before each tests
     */
    beforeEach(async(() => {
        //Inject configuration before every test
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                HttpClientModule,
                CommonModule,
                FormsModule,
                NgReduxModule,
                RouterTestingModule
            ],
            providers: [VATService],
            declarations: [
                AppComponent,
                SearchComponent,
                NotificationComponent,
                DetailsComponent,
            ]
        })
            .compileComponents().then(() => {
                fixture = TestBed.createComponent(DetailsComponent);
                component = fixture.componentInstance;
            });
    }));
    it('should create the app - DetailsComponent', async(() => {
        expect(component).toBeTruthy();
    }));
});

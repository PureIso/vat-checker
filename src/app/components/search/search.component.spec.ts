
//==================================================================================================//
//==================================================================================================//
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgReduxModule } from '@angular-redux/store';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { VATService } from '../../services/vat.service';
import { SearchComponent } from '../../components/search/search.component';
import { DetailsComponent } from '../details/details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../../app.component';
import { NotificationComponent } from '../notification/notification.component';
//==================================================================================================//
//==================================================================================================//
/**
 * SearchComponent - Test Suite
 */
describe('SearchComponent', () => {
    let fixture: ComponentFixture<SearchComponent>;
    let component: SearchComponent;
    /**
     * Setup - Called before each tests
     */
    beforeEach(async(() => {
        //Inject configuration before every test
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                HttpClientModule,
                CommonModule,
                FormsModule,
                NgReduxModule,
                RouterTestingModule
            ],
            providers: [VATService],
            declarations: [
                AppComponent,
                SearchComponent,
                NotificationComponent,
                DetailsComponent,
            ]
        })
            .compileComponents().then(() => {
                fixture = TestBed.createComponent(SearchComponent);
                component = fixture.componentInstance;
            });
    }));
    it('should create the app - SearchComponent', async(() => {
        expect(component).toBeTruthy();
    }));
    it('should be able to search for vat', async(() => {
        spyOn(component, 'onSearchVAT');
        let button = fixture.debugElement.nativeElement.querySelector('button');
        button.click();
        fixture.whenStable().then(() => {
            expect(component.onSearchVAT).toHaveBeenCalled();
        })
    }));
});

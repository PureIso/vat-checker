import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { VATService } from '../../services/vat.service';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import * as NotificationActions from "../../actions/notification.actions";
import * as SearchFilterActions from "../../actions/search-filter.actions";
import { AppState } from '../../store/app.state';
import { SearchFilter } from '../../models/search.model';
import { ToastContainer } from '../../models/toast.model';
import { toast } from '../../models/toast.interface';

import './search.component.sass';
import { vat } from 'app/models/vat.interface';


@Component({
    selector: "search-component",
    templateUrl: './search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class SearchComponent implements OnInit {
    vatSearchInput: string;
    vatSearchButtonDisabled: boolean = false;

    @select('searchFilter') searchFilter$: Observable<SearchFilter>;
    searchFilter: SearchFilter;
    @select('notificationContainer') notificationContainer$: Observable<ToastContainer>;
    notificationContainer: ToastContainer;

    constructor(private vatService: VATService, private ngRedux: NgRedux<AppState>) { }

    ngOnInit() {
        this.searchFilter$.subscribe((x: SearchFilter) => this.searchFilter = x);
        this.notificationContainer$.subscribe((x: ToastContainer) => this.notificationContainer = x);
    }

    updateSearchFilter(searchFilter: SearchFilter) {
        let searchFilterActions: SearchFilterActions.Actions = new SearchFilterActions.UpdateSearchedVAT(searchFilter);
        this.ngRedux.dispatch({ type: searchFilterActions.type, payload: searchFilterActions.payload });
    }

    updateNotifications(notification: toast) {
        if (this.notificationContainer.notifications.length >= 5) {
            this.notificationContainer.notifications.splice(0, 1);
        };
        this.notificationContainer.notifications.push(notification);
        let notificationContainerPayload: ToastContainer = {
            notifications: this.notificationContainer.notifications,
        };
        let notificationActions: NotificationActions.Actions = new NotificationActions.updateNotification(notificationContainerPayload);
        this.ngRedux.dispatch({ type: notificationActions.type, payload: notificationActions.payload });
    }

    onSearchVAT() {
        this.vatSearchButtonDisabled = true;

        if (this.vatSearchInput == undefined || this.vatSearchInput == "") {
            let notificationPayload: toast = {
                id: new Date().getUTCMilliseconds().toString(),
                content: "Pleas enter a valid VAT.",
                dismissed: false,
                style: "danger"
            };
            this.updateNotifications(notificationPayload);
            return;
        };

        let notificationPayload: toast = {
            id: new Date().getUTCMilliseconds().toString(),
            content: "Searching for: " + this.vatSearchInput,
            dismissed: false,
            style: "info"
        };
        this.updateNotifications(notificationPayload);

        this.vatService.getVATInformationByNumber(this.vatSearchInput)
            .subscribe(
                (result: vat) => this.searchFilter.currentVAT = result,
                (error: any) => {
                    this.vatSearchButtonDisabled = false;
                    let payload: toast = {
                        id: new Date().getUTCMilliseconds().toString(),
                        content: error['status'] + ' - ' + error['statusText'],
                        dismissed: false,
                        style: "danger"
                    };
                    this.updateNotifications(payload);
                },
                () => {
                    this.vatSearchButtonDisabled = false;
                    if (this.searchFilter.currentVAT != undefined) {
                        let searchFilterPayload: SearchFilter = {
                            currentVAT: this.searchFilter.currentVAT,
                        };
                        this.updateSearchFilter(searchFilterPayload);
                        let notificationPayload: toast = {
                            id: new Date().getUTCMilliseconds().toString(),
                            content: searchFilterPayload.currentVAT.VATNumber,
                            dismissed: false,
                            style: "success"
                        };
                        this.updateNotifications(notificationPayload);
                    };
                }
            );
    }
}
import { Action } from 'redux';
import { SearchFilter } from 'app/models/search.model';

//Action types
export const UPDATEVATINFORMATION = 'UPDATEVATINFORMATION';

//Class for each of our actions
export class UpdateSearchedVAT implements Action {
    readonly type = UPDATEVATINFORMATION;
    //Allows us to pass in data
    constructor(public payload: SearchFilter) { }
}

export type Actions = UpdateSearchedVAT;
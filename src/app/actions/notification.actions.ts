import { Action } from 'redux';
import { ToastContainer } from 'app/models/toast.model';

//Action types
export const UPDATENOTIFICATION = 'UPDATENOTIFICATION';

//Class for each of our actions
export class updateNotification implements Action {
    readonly type = UPDATENOTIFICATION;
    //Allows us to pass in data
    constructor(public payload: ToastContainer) { }
}

export type Actions = updateNotification;
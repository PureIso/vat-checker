//==================================================================================================//
//==================================================================================================//
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { vat } from '../models/vat.interface';
//==================================================================================================//
//==================================================================================================//
//Interface to the business layer
@Injectable()
export class VATService {
    baseURL = "https://vat.erply.com/";

    constructor(private httpClient: HttpClient) { }
    /**
     * Get a specified VAT information using VAT Number
     */
    getVATInformationByNumber(vatNumber: string): Observable<vat> {
        //https://vat.erply.com/numbers?vatNumber=BG999999999
        let query = this.baseURL + 'numbers?vatNumber=' + vatNumber;
        return this.httpClient.get<vat>(query);
    }
}
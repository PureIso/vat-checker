//==================================================================================================//
//==================================================================================================//
import { TestBed, async, inject } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgReduxModule } from '@angular-redux/store';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { VATService } from './vat.service';
import { vat } from '../models/vat.interface';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../app.component';
import { SearchComponent } from '../components/search/search.component';
import { DetailsComponent } from '../components/details/details.component';
import { NotificationComponent } from '../components/notification/notification.component';
//==================================================================================================//
//==================================================================================================//
/**
 * VATService - Test Suite
 */
describe('VATService', () => {
    let mockVATNumber: string;
    let mockVAT: vat;

    /**
     * Setup - Called before each tests
     */
    beforeEach(async(() => {
        //Inject configuration before every test
        TestBed.configureTestingModule({
            imports: [
                BrowserModule,
                HttpClientModule,
                CommonModule,
                FormsModule,
                NgReduxModule,
                RouterTestingModule
            ],
            providers: [VATService],
            declarations: [
                AppComponent,
                SearchComponent,
                NotificationComponent,
                DetailsComponent,
            ]
        })
            .compileComponents().then(() => {
                jasmine.DEFAULT_TIMEOUT_INTERVAL = 50000;
                this.mockVATNumber = "BG999999999";
                this.mockVAT = {
                    VATNumber: "999999999",
                    CountryCode: "BG",
                    RequestDate: new Date(),
                    Valid: false,
                    Name: "---",
                    Address: "---"
                }
            });
    }));
    it('VATService should be created', async(inject([VATService], (service: VATService) => {
        expect(service).toBeTruthy();
    })));
    it('VATService should have getVATInformationByNumber function', async(inject([VATService], (service: VATService) => {
        expect(service.getVATInformationByNumber).toBeTruthy();
    })));
    // it('VATService.getVATInformationByNumber should return vat information', inject([VATService],
    //     async (service: VATService) => {
    //         const result = await service.getVATInformationByNumber(this.mockVATNumber).toPromise();
    //         expect(result).toBeTruthy();
    //     }));
    // it('VATService.getVATInformationByNumber result should match the mock value', inject([VATService],
    //     async (service: VATService) => {
    //         const result = await service.getVATInformationByNumber(this.mockVATNumber)
    //             .toPromise();
    //         expect(result.Address).toEqual(this.mockVAT.Address);
    //         expect(result.CountryCode).toEqual(this.mockVAT.CountryCode);
    //         expect(result.Name).toEqual(this.mockVAT.Name);
    //         expect(result.VATNumber).toEqual(this.mockVAT.VATNumber);
    //         expect(result.Valid).toEqual(this.mockVAT.Valid);
    //     }));
});

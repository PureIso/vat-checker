//==================================================================================================//
//==================================================================================================//
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { vat } from '../models/vat.interface';
//==================================================================================================//
//==================================================================================================//
export class Message {
    content: string;
    style: string;
    dismissed: boolean = false;

    constructor(content: string, style?: string) {
        this.content = content;
        this.style = style || 'info';
    }
}


//Interface to the business layer
@Injectable()
export class ToastService {
    constructor() { }

    getMessage() {
        return "Message";
    }

    sendMessage(content: string, style: string) {
        const message = new Message(content, style);
    }

    dismissMessage(messageKey: string) {

    }
}
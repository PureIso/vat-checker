import { SearchFilter } from "../models/search.model";
import { ToastContainer } from "../models/toast.model";
import * as SearchFilterReducer from "../reducers/search-filter.reducer";
import * as NotificationContainerReducer from "../reducers/notification.reducer";


export interface AppState {
    readonly searchFilter: SearchFilter;
    readonly notificationContainer: ToastContainer;
}

export const INITIALSTATE: AppState = {
    searchFilter: SearchFilterReducer.initialState,
    notificationContainer: NotificationContainerReducer.initialState
}
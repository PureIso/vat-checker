import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgRedux, NgReduxModule } from '@angular-redux/store';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { VATService } from './services/vat.service';
import { SearchComponent } from './components/search/search.component';
import { DetailsComponent } from './components/details/details.component';
import { NotificationComponent } from './components/notification/notification.component';
import { AppComponent } from './app.component';
import { AppState } from './store/app.state';
import { store } from './reducers/reducers';

import '../index.sass';



@NgModule({
	imports: [
		BrowserModule,
		HttpClientModule,
		CommonModule,
		FormsModule,
		NgReduxModule,
		BrowserAnimationsModule
	],
	declarations: [
		SearchComponent,
		DetailsComponent,
		NotificationComponent,
		AppComponent
	],
	providers: [VATService],
	bootstrap: [AppComponent]
})

export class AppModule {
	constructor(ngRedux: NgRedux<AppState>) {
		//Initial state of our store
		ngRedux.provideStore(store);
	}
}
import { ToastContainer } from "../models/toast.model";
import * as NotificationActions from "../actions/notification.actions";

export const initialState: ToastContainer = {
    notifications: []
}

export function notificationReducer(state: ToastContainer = initialState, action: NotificationActions.Actions): ToastContainer {
    switch (action.type) {
        case NotificationActions.UPDATENOTIFICATION:
            return action.payload;
        default:
            return state;
    }
};
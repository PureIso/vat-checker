import { combineReducers, createStore } from 'redux'
import { searchReducer } from './search-filter.reducer';
import { notificationReducer } from './notification.reducer';
import { AppState, INITIALSTATE } from '../store/app.state';

export const combinedReducers = combineReducers<AppState>({ searchFilter: searchReducer, notificationContainer: notificationReducer });
export const store = createStore(combinedReducers, INITIALSTATE);
import { SearchFilter } from "../models/search.model";
import * as SearchFilterActions from "../actions/search-filter.actions";

export const initialState: SearchFilter = {
    currentVAT: {
        VATNumber: "---",
        CountryCode: "---",
        RequestDate: "---",
        Valid: false,
        Name: "---",
        Address: "---"
    }
}

export function searchReducer(state: SearchFilter = initialState, action: SearchFilterActions.Actions): SearchFilter {
    switch (action.type) {
        case SearchFilterActions.UPDATEVATINFORMATION:
            return action.payload;
        default:
            return state;
    }
};
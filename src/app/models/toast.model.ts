import { toast } from "app/models/toast.interface";

export interface ToastContainer {
    notifications: toast[],
}
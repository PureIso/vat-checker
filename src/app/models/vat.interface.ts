export interface vat {
    VATNumber: string,
    CountryCode: string,
    RequestDate: string,
    Valid: boolean,
    Name: string,
    Address: string
}
import { vat } from "app/models/vat.interface";

export interface SearchFilter {
    currentVAT: vat,
}
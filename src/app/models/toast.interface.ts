export interface toast {
    id: string;
    content: string;
    style: string;
    dismissed: boolean;
}

// Import the core angular services.
import { Component, ViewEncapsulation } from "@angular/core";
import './app.component.sass';

@Component({
	selector: "my-app",
	templateUrl: "./app.component.html",
	encapsulation: ViewEncapsulation.None
})
export class AppComponent { }
# VAT Checker - Frontend Stack

Lookup VAT information.
**Version: 1.0.0**

## Features

- Lookup VAT number

## Technologies

- [Angular](https://angular.io/) - (Framework)
- [TypeScript](https://www.typescriptlang.org/) - (Language)
- [Webpack](https://webpack.js.org/) - (Build Tool)
- [npm](https://www.npmjs.com/) - (Package Manager)
- [Karma](http://karma-runner.github.io/0.12/index.html) (Test Runner)
- [Rxjs](https://github.com/ReactiveX/rxjs)
- [Sass](http://sass-lang.com/)
- [Redux](https://redux.js.org/)
- [Bootstrap](http://getbootstrap.com/)
- [Font Awesome](https://fontawesome.com/)

## Installation

To start the server, we need to install the dependencies and devDependencies.
For development environments mode:

```sh
npm install
npm start
```

Karma Unit Test Runner

```sh
npm test
```

## License

[MIT](https://gitlab.com/PureIso/vat-checker/blob/master/LICENSE)

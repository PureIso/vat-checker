const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    resolve: {
        extensions: ['.js', '.ts']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    {
                        loader: 'awesome-typescript-loader',
                        query: {
                            /**
                             * Use inline sourcemaps for "karma-remap-coverage" reporter
                             */
                            sourceMap: false,
                            inlineSourceMap: true,
                            compilerOptions: {
                                /**
                                 * Remove TypeScript helpers to be injected
                                 * below by DefinePlugin
                                 */
                                removeComments: true
                            }
                        }
                    },
                    'angular2-template-loader'
                ],
                exclude: [/\.e2e\.ts$/]
            },
            {
                test: /\.(html)$/,
                use: ['raw-loader']
            },
            {
                /**
                 * Dealing with the Warning:
                 * System.import() is deprecated and will be removed soon. Use import() instead.
                 * 
                 * Mark files inside `@angular/core` as using SystemJS style dynamic imports.
                 * Removing this will cause deprecation warnings to appear.
                 */
                test: /[\/\\]@angular[\/\\]core[\/\\].+\.js$/,
                parser: { system: true },  // enable SystemJS
            },
            {
                test: /\.(sc|sa)ss$/,
                exclude: /node_modules/,
                loaders: ['raw-loader', 'sass-loader'] // sass-loader not scss-loader
            }
        ]
    },
    plugins: [
        /** 
         * Dealing with the Warning:
         * WARNING in ./node_modules/@angular/core/fesm5/core.js
         * Critical dependency: the request of a dependency is an expression
         */
        new webpack.ContextReplacementPlugin(
            /\@angular(\\|\/)core(\\|\/)fesm5/,
            path.resolve(__dirname, '../src')
        )
    ]
}

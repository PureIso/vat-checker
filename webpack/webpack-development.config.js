const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const AngularCompilerPlugin = require("@ngtools/webpack").AngularCompilerPlugin;
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: {
        main: './src/main.ts'
    },
    output: {
        path: path.join(__dirname, "../dist/"),
        filename: '[name].[contenthash].js',
    },
    resolve: {
        extensions: ['.js', '.ts']
    },
    devServer: {
        contentBase: path.join(__dirname, "../dist/"),
        port: 9000
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            // I provide a TypeScript compiler that performs Ahead of Time (AoT)
            // compilation for the Angular application and TypeScript code.
            {
                test: /(\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
                loader: "@ngtools/webpack"
            },
            {
                test: /\.ts$/,
                use: [
                    {
                        loader: 'awesome-typescript-loader',
                        query: {
                            /**
                             * Use inline sourcemaps for "karma-remap-coverage" reporter
                             */
                            sourceMap: false,
                            inlineSourceMap: true,
                            compilerOptions: {
                                /**
                                 * Remove TypeScript helpers to be injected
                                 * below by DefinePlugin
                                 */
                                removeComments: true
                            }
                        }
                    },
                    'angular2-template-loader'
                ],
                exclude: [/\.e2e\.ts$/]
            },
            {
                // Mark files inside `@angular/core` as using SystemJS style dynamic imports.
                // Removing this will cause deprecation warnings to appear.
                test: /[\/\\]@angular[\/\\]core[\/\\].+\.js$/,
                parser: { system: true },
            },
            // When the @ngtools webpack loader runs, it will replace the @Component()
            // "templateUrl" and "styleUrls" with inline "require()" calls. As such, we
            // need the raw-loader so that require() will know how to load .html and .css
            // files as plain-text.
            {
                test: /\.(html)$/,
                use: ['raw-loader']
            },
            {
                test: /\.(sc|sa|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            minimize: true,
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader"
                    }
                ]
            },
            {
                test: /\.(jpg|ico|otf|gif|png|jpe?g)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'img/',
                            publicPath: 'img/'
                        }
                    }
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'url-loader?limit=10000&name=fonts/[name].[ext]'
            }
        ]
    },
    devServer: {
        historyApiFallback: true,
    },
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 0,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module) {
                        // get the name. E.g. node_modules/packageName/not/this/part.js
                        // or node_modules/packageName
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        // npm package names are URL-safe, but some servers don't like @ symbols
                        return `npm.${packageName.replace('@', '')}`;
                    },
                },
            },
        },
    },
    plugins: [
        new AngularCompilerPlugin({
            tsConfigPath: path.join(__dirname, "../tsconfig.json"),
            mainPath: path.join(__dirname, "../src/main"),
            entryModule: path.join(__dirname, "../src/app/app.module#AppModule"),
            // Webpack will generate source-maps independent of this setting. But,
            // this setting uses the original source code in the source-map, rather
            // than the generated / compiled code.
            sourceMap: true,
            //skipCodeGeneration: true
        }),
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "index.html",
            showErrors: true,
            title: "Webpack App",
            path: path.join(__dirname, "../dist/"),
            hash: true
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new CopyWebpackPlugin([
            { from: '**/*.jpg', to: 'img/[name].[ext]' },
            { from: '**/*.ico', to: 'img/[name].[ext]' }
        ]),
        new webpack.HashedModuleIdsPlugin(), // so that file hashes don't change unexpectedly
        new webpack.ContextReplacementPlugin(
            /\@angular(\\|\/)core(\\|\/)fesm5/,
            path.resolve(__dirname, '../src')
        )
    ]

}
